import requests
import json

def SendSMS(reqUrl, apiKey, secretKey, useType, phoneNo, senderId, textMessage):
	req_params = {
	'apikey': apiKey,
	'secret': secretKey,
	'usetype': useType,
	'phone': phoneNo,
	'senderid': senderId,
	'message': textMessage
	}
	
	return requests.post(reqUrl, req_params)

