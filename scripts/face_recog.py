from scipy.spatial import distance as dist
from imutils.video import FileVideoStream
from imutils.video import VideoStream
from imutils import face_utils
import face_recognition
import numpy as np
import argparse
import imutils
import time
import dlib
import cv2
import os
import keyboard
import multiprocessing as mp
from functools import partial
import sys
import time
import firebase_admin
from firebase_admin import credentials
from firebase_admin import auth
from firebase_admin import db
import json
from datetime import datetime
import tkinter as tk
from tkinter import messagebox
import pickle
import MailSender
import smsSender
from pyfcm import FCMNotification

def eye_aspect_ratio(eye):
    A = dist.euclidean(eye[1], eye[5])
    B = dist.euclidean(eye[2], eye[4])

    C = dist.euclidean(eye[0], eye[3])

    ear = (A + B) / (2.0 * C)

    return ear

def detect_blink():
	EYE_AR_THRESH = 0.3
	EYE_AR_CONSEC_FRAMES = 3

	COUNTER = 0
	TOTAL = 0

	print("[INFO] loading facial landmark predictor...")
	detector = dlib.get_frontal_face_detector()
	predictor = dlib.shape_predictor("../utils/shape_predictor_68_face_landmarks.dat")

	(lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
	(rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

	print("[INFO] starting video stream thread...")
	#URL = "http://localhost:4747/mjpegfeed"
	#vs = FileVideoStream("http://192.168.31.216:8080/video").start()
	#vs = FileVideoStream("http://192.168.31.216:8080/video").start()
	vs = FileVideoStream(0).start()

	fileStream = True

	time.sleep(1.0)

	flag = 0
	flag1 = 0

	while True:

		frame = vs.read()

		frame = imutils.resize(frame, width=500)
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

		rects = detector(gray, 0)

		if keyboard.is_pressed("s") or flag1 == 1:

			flag1 = 1

			for rect in rects:
				shape = predictor(gray, rect)
				shape = face_utils.shape_to_np(shape)

				leftEye = shape[lStart:lEnd]
				rightEye = shape[rStart:rEnd]
				leftEAR = eye_aspect_ratio(leftEye)
				rightEAR = eye_aspect_ratio(rightEye)

				ear = (leftEAR + rightEAR) / 2.0

				leftEyeHull = cv2.convexHull(leftEye)
				rightEyeHull = cv2.convexHull(rightEye)
				cv2.drawContours(frame, [leftEyeHull], -1, (0, 255, 0), 1)
				cv2.drawContours(frame, [rightEyeHull], -1, (0, 255, 0), 1)

				if ear < EYE_AR_THRESH:
					COUNTER += 1

				else:
					if COUNTER >= EYE_AR_CONSEC_FRAMES:
						TOTAL += 1
						flag = 1

					COUNTER = 0

				cv2.putText(frame, "Blinks: {}".format(TOTAL), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
				cv2.putText(frame, "EAR: {:.2f}".format(ear), (300, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

		cv2.imshow("Frame", frame)
		key = cv2.waitKey(1) & 0xFF

		if key == ord("q") or flag == 1:
			break

	cv2.destroyAllWindows()
	return frame

def face_recognize(face):
	all_face_encodings = {}
	image_to_be_matched_encoded = face_recognition.face_encodings(face)[0]

	with open('dataset_faces.dat', 'rb') as f:
		all_face_encodings = pickle.load(f)

	face_names = list(all_face_encodings.keys())
	face_encodings = np.array(list(all_face_encodings.values()))
	result = face_recognition.compare_faces(face_encodings, image_to_be_matched_encoded, 0.47)
	names_with_result = list(zip(face_names, result))
	return names_with_result


def init_firebase():
	cred = credentials.Certificate("../utils/facepay-19718-firebase-adminsdk-o9f9l-85524f3928.json")
	firebase_admin.initialize_app(cred, {'databaseURL': 'https://facepay-19718.firebaseio.com'})

def get_user_cart_amount(userId):
	print("User ID : " + userId)
	ref = db.reference('UserCart/'+userId)
	cart_items = ref.get()
	cart_amount = 0
	print(cart_items)
	for item in cart_items:
		item_price = cart_items[item]['Price']
		item_quantity = cart_items[item]['Quantity']
		cart_amount = cart_amount + (item_price*item_quantity)

	return cart_amount

def get_user_wallet_balance(userId):
	print(userId)
	ref = db.reference('Users/'+userId)
	user = ref.get()
	return user['Balance']

def perform_transaction(userId, wallet_balance, cart_amount):

	user = auth.get_user(userId)
	email_id = user.email
	
	ref = db.reference("Users/"+userId)
	ref.update({
		'Balance': wallet_balance - cart_amount
	})

    # create new transaction
	now = datetime.now()
	current_date = now.strftime("%B %d, %Y")
	current_time = now.strftime("%I:%M %p")
	ref = db.reference("Transactions/" + userId)
	key = ref.push({
		'Amount': cart_amount,
		'Closing Balance': wallet_balance - cart_amount,
		'Date': current_date,
		'Purpose': "CartDebit",
		'Time': current_time
	}).key

    # create history table and add current cart items and transaction id
	ref = db.reference("History/"+userId)
	ref1 = db.reference("UserCart/"+userId)
	data = ref1.get()
	ref.push({
		'Transaction Id': key,
		'Cart': data
	})


    # delete current cart items
	ref = db.reference("UserCart/"+userId)
	ref.delete()

	messagebox.showinfo("FacePay", "Transaction Successful")
	MailSender.SendMail(email_id, cart_amount)
	ref = db.reference("Users/" + userId)
	data = ref.get()
	
	push_service = FCMNotification(api_key="AAAAr5Au9i4:APA91bH7zQl3lLAjj0Ie-1qm4T2LKB_UdzLxt15uRjj-rKUnLHH-HrLjitihHWhzGvCzkewsLlCwSYgNhhxGzClsh2iC76bywqI47IsQqrsmmWG3KvZ5ImSz7bA9bVIKSlWpZd0WWej9")
	
	reg_id = data['Token']
	title = "Transaction Successfully Completed"
	body = "You've purchased items from FaceMart worth %s. Thank you & visit again!" %(cart_amount)
	result = push_service.notify_single_device(registration_id=reg_id, message_body=body, message_title=title)
	
	url = 'https://www.sms4india.com/api/v1/sendCampaign'
	api_key_sms = '732GB5Q18FPV6H6EBN916ADK2KU4QCNG'
	secret_key = 'W2AABH8D4M4W7TMZ'
	use_type = 'stage'
	ph_no = data['Contact']
	sender_id = 'FACEPAY'

	smsSender.SendSMS(url, api_key_sms, secret_key, use_type, ph_no, sender_id, body)	

def main():
	
	#generate_encoding()
	my_face = detect_blink()
	cv2.imwrite("myface.jpg", my_face)
	start_time = time.time()
	results = face_recognize(my_face)

	print("--- %s seconds ---" % (time.time() - start_time))
	i = len(results)
		
	for res in results:
		print(res)
		if res[1] == True:
			userId = res[0][:-4]
			init_firebase()
			cart_amount = get_user_cart_amount(userId)
			wallet_balance = get_user_wallet_balance(userId)
			print(wallet_balance)
			if cart_amount < wallet_balance:
				perform_transaction(userId, wallet_balance, cart_amount)
			else:
				messagebox.showinfo("FacePay", "Insufficient Balance")
			
			break
			
		i -= 1
		
	if i==0:
		messagebox.showinfo("FacePay", "Not Recognized")

	
	hsv = cv2.cvtColor(my_face, cv2.COLOR_BGR2HSV)
	# print(hsv)

if __name__ == '__main__':
    root = tk.Tk()
    root.title("FacePay")
    
    btn = tk.Button(root, text="Pay Now", command=main)
    btn.pack()
    
    root.mainloop()
