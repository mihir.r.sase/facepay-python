import os
import requests
from sys import *
from urllib.parse import urlparse
import face_recognition
import pickle


def is_downloadable(url):
	h = requests.head(url, allow_redirects=True)
	header = h.headers
	content_type = header.get('content-type')
	
	if 'text' in content_type.lower():
		print("text")
		return False
	
	if 'html' in content_type.lower():
		print("html")
		return False
		
	return True
	
def get_filename_by_cd(url):
	a = urlparse(url)
	return os.path.basename(a.path)

def update_face_dataset(image):
	all_face_encodings = {}
	with open('dataset_faces.dat', 'rb') as f:
		all_face_encodings = pickle.load(f)
		
	current_image = face_recognition.load_image_file("../project_images/" + image)
	current_image_encoded = face_recognition.face_encodings(current_image)[0]
	all_face_encodings[image] = current_image_encoded

	with open('dataset_faces.dat', 'wb') as f:
		pickle.dump(all_face_encodings, f)

def URLDownload(url):
	
	allowed = is_downloadable(url)
	print(allowed)
	if allowed:
	
		try:
			res = requests.get(url, allow_redirects=True)
			
			filename = get_filename_by_cd(url)

			ss = "../project_images/" + filename + ".jpg"
			
			fd = open(ss, "wb")		
		
			for buffer in res.iter_content(1024):
				fd.write(buffer)
			
			fd.close()

			update_face_dataset(filename + ".jpg")
			
			return True
			
		except Exception as e:
			print(e)
			return False
		
	else:
		return False

# def main():
	
# 	url = 'https://storage.googleapis.com/glide-d0c55.appspot.com/images/YOM0uZJ3oQSCW0Lh8OZF75YsVle2?Expires=1569823037&GoogleAccessId=firebase-adminsdk-zyk2l%40glide-d0c55.iam.gserviceaccount.com&Signature=hVwr4d5sX9IWLtBL1tQJ%2BFmSBuI1m8VxU3uqJX0Evk7wk%2FWalwMd1s4hePZrsrWf4aVep%2BQgVC7OH28FEEO6r%2F8kVS%2B2ZIVna0Np8Us%2F27yqAYzX4iGE2dlC9SNKC2ZDA%2Fe30mLSxhycYrn7STS%2F7Iy8ZWBXwApZ4vvtr%2Bc87DPEtbb0t7UdtMWvaq%2B9Z2XxnBufqF2qUPPkax%2Bap1thsIcK72Wtbepbr39loUmF7GWVhxpfw7Si7pwDKEmLMrJrF5sw3IVs%2F5j0UMdPxR5BVkzSe5x1%2Bxw14aPqVf5d%2Fgs9nEW%2FHriozb1r3lJh4rvoOiSEBDVuUAClF20tLrtx1Q%3D%3D'
	
# 	result = URLDownload(url)

# 	if result:
# 		print("Downloaded successfully")
		
# 	else:
# 		print("Download unsuccessful")	
	
# if __name__ == "__main__":
# 	main()
