import os 
import time
import psutil
import smtplib
import schedule
import urllib.request
from sys import *
from datetime import datetime
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart

def SendMail(emailid, amt):

	try:
		fromaddr = "facepay20@gmail.com"
		toaddr = emailid
		
		msg = MIMEMultipart()
		
		msg['From'] = fromaddr
		msg['To'] = toaddr
		
		body = """
		Hey %s
		
		You've purchased items worth %s using FacePay App.
		Thank you for shopping at FaceMart and visit again! 
		
		""" %(toaddr, amt)

		subject = """
		Transaction Completed Successfully
		"""
		
		msg['Subject'] = subject
		
		msg.attach(MIMEText(body, 'plain'))
		
		s = smtplib.SMTP('smtp.gmail.com', 587)
		s.starttls()
		s.login(fromaddr, "beproject2020")
		text = msg.as_string()
		s.sendmail(fromaddr, toaddr, text)
		s.quit()		
		
		print("Mail Successfully Sent")
		
	except Exception as e:
		print("Mail not sent : ", e)
