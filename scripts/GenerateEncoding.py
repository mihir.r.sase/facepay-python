from scipy.spatial import distance as dist
from imutils.video import FileVideoStream
from imutils.video import VideoStream
from imutils import face_utils
import face_recognition
import numpy as np
import argparse
import imutils
import time
import dlib
import cv2
import os
import keyboard
import multiprocessing as mp
from functools import partial
import sys
import time
import firebase_admin
from firebase_admin import credentials
from firebase_admin import auth
from firebase_admin import db
import json
from datetime import datetime
import tkinter as tk
from tkinter import messagebox
import pickle

def generate_encoding():
	all_face_encodings = {}
	images = os.listdir('../project_images')
	print(images)
	for image in images:
		print(image)
		if image != ".DS_Store":
			current_image = face_recognition.load_image_file("../project_images/" + image)
			current_image_encoded = face_recognition.face_encodings(current_image)[0]
			all_face_encodings[image] = current_image_encoded

	with open('dataset_faces.dat', 'wb') as f:
		pickle.dump(all_face_encodings, f)
		
generate_encoding()
